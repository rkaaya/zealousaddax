package com.rkaaya.zealous.addax.languageprop;

public enum LanguageEntry {
    APPLE("apple"), CAR("car");

    private String word;

    public String getWord() {
        return this.word;
    }

    private LanguageEntry(final String word) {
        this.word = word;
    }
}
