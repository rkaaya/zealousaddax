package com.rkaaya.zealous.addax.languageprop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class MultiLanguageService {

    private static Logger log = LoggerFactory.getLogger(MultiLanguageService.class);

    private Properties prop;

    public MultiLanguageService(final Language lang) throws IOException {
        InputStream inputStream = InputStream.nullInputStream();
        try {
            prop = new Properties();
            final String langFileName = "lang_" + lang.toString().toLowerCase() + ".properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(langFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("Language file '" + langFileName + "' not found in the classpath");
            }
        } catch (Exception e) {
            log.error("Exception: ", e);
        } finally {
            if(inputStream != null) {
                inputStream.close();
            }
        }
    }

    public String getWord(final LanguageEntry entry) {
        return prop.getProperty(entry.getWord());
    }
}
