package com.rkaaya.zealous.addax.runnable;

import com.rkaaya.zealous.addax.languageprop.Language;
import com.rkaaya.zealous.addax.languageprop.LanguageEntry;
import com.rkaaya.zealous.addax.languageprop.MultiLanguageService;
import com.rkaaya.zealous.addax.lottery.ColumnLotteryService;
import com.rkaaya.zealous.addax.lottery.LotteryService;
import com.rkaaya.zealous.addax.lottery.PatternLotteryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ZealousAddax {

    private static Logger log = LoggerFactory.getLogger(ZealousAddax.class);

    public static void main(String[] args) throws IOException {
        LotteryService patternBased = new PatternLotteryService();
        log.info("Pattern based lottery service parsed {} rows.", patternBased.parseLottery().size());

        LotteryService columnBased = new ColumnLotteryService();
        log.info("Column based lottery service parsed {} rows.", columnBased.parseLottery().size());

        MultiLanguageService languageServiceEng = new MultiLanguageService(Language.ENG);
        MultiLanguageService languageServiceHun = new MultiLanguageService(Language.HUN);

        log.info("English: {}", languageServiceEng.getWord(LanguageEntry.APPLE));
        log.info("Hungary: {}", languageServiceHun.getWord(LanguageEntry.APPLE));
    }


}
