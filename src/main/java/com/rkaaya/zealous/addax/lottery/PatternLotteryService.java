package com.rkaaya.zealous.addax.lottery;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class PatternLotteryService implements LotteryService {

    @Override
    public List<int[]> parseLottery() throws FileNotFoundException {
        final Pattern datePattern = Pattern.compile("(([0-9]{4};[0-9][0-9]?))");
        final Pattern numsPattern = Pattern.compile("(;[0-9][0-9]?){5}");

        final InputStream csv = this.getClass().getClassLoader()
                .getResourceAsStream("lottery/lotteryotos.csv");
        final Scanner scn = new Scanner(csv);
        final List<String> data = new ArrayList<>();

        while (scn.hasNext()) {
            //Find date and numbers
            data.add(scn.findInLine(datePattern).concat(scn.findInLine(numsPattern)));
            scn.nextLine();
        }

        //Parse them into array
        return data.stream()
                .map(x -> Arrays.stream(x.split(";"))
                        .filter(c -> !c.isEmpty())
                        .mapToInt(Integer::parseInt)
                        .toArray()).toList();
    }
}
