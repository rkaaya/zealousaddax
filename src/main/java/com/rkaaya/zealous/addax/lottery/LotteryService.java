package com.rkaaya.zealous.addax.lottery;

import java.io.FileNotFoundException;
import java.util.List;

public interface LotteryService {
    List<int[]> parseLottery() throws FileNotFoundException;
}
