package com.rkaaya.zealous.addax.lottery;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ColumnLotteryService implements LotteryService {

    @Override
    public List<int[]> parseLottery() throws FileNotFoundException {
        final InputStream csv = this.getClass().getClassLoader()
                .getResourceAsStream("lottery/lotteryotos.csv");
        final Scanner scn = new Scanner(csv);

        final List<int[]> result = new ArrayList<>();

        while (scn.hasNext()) {
            result.add(parseData(scn.nextLine().split(";")));
        }

        return result;
    }

    private int[] parseData(String[] data) {
        return new int[]{
                Integer.parseInt(data[LotteryColumn.YEAR.getColumn()]),
                Integer.parseInt(data[LotteryColumn.WEEK.getColumn()]),
                Integer.parseInt(data[LotteryColumn.NUM1.getColumn()]),
                Integer.parseInt(data[LotteryColumn.NUM2.getColumn()]),
                Integer.parseInt(data[LotteryColumn.NUM3.getColumn()]),
                Integer.parseInt(data[LotteryColumn.NUM4.getColumn()]),
                Integer.parseInt(data[LotteryColumn.NUM5.getColumn()])
        };
    }
}
