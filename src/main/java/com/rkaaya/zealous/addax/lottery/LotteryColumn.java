package com.rkaaya.zealous.addax.lottery;

public enum LotteryColumn {
    YEAR(0), WEEK(1), NUM1(11), NUM2(12), NUM3(13), NUM4(14), NUM5(15);

    private Integer column;

    public Integer getColumn() {
        return this.column;
    }

    private LotteryColumn(final Integer column) {
        this.column = column;
    }
}
